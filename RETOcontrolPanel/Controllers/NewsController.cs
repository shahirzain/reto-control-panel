﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Entities;
using RETOcontrolPanel.Models;

namespace RETOcontrolPanel.Controllers
{
    public class NewsController : Controller
    {
        private Model1 db = new Model1();
        NewsViewModel img = new NewsViewModel();

        // GET: News
        public ActionResult Index()
        {
            var news = db.News.Where(n => n.IsActive == true).Include(n => n.News_body).ToList();
            return View(news.Select(x => new IndexModel(x)).ToList());
        }

        // GET: News/Details/5
        public ActionResult Details(int? id)
        {
           // var obj = db.News.Where(u => u.ImgURL == img.ImgURL).FirstOrDefault();
           //newsView.News_body.body = HttpUtility.HtmlDecode(newsView.News_body.body);
            if (id == null)
            {

               
                //newsView.ImgURL = obj.ImgURL;
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Include("News_body").SingleOrDefault(x => x.News_Id == id);
            if (news == null)
            {
                return HttpNotFound();
            }
            news.News_body.body = HttpUtility.HtmlDecode(news.News_body.body);
            return View(news);
        }

        // GET: News/Create
        public ActionResult Create()
        {
            ViewBag.News_Id = new SelectList(db.News_body, "News_Id", "body");
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "News_Id,Title,content,Body,ImgURL")] RETOcontrolPanel.Models.NewsViewModel news)
        
{
            try
            { 
                string fileName = "";
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];
                    if (file != null && file.ContentLength > 0)
                    {
                        fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                        file.SaveAs(path);
                    }
                }
                if (ModelState.IsValid)
                {
                    
                    news.ShowImage = true;
                    //news.Time_Created = System.DateTime.Now;
                    news.ImgURL = fileName;
                    //news.CreatedBy = "System";
                    //news.CreatedDate = System.DateTime.Today;
                    news.IsActive = true;

                    string content = news.Body.Replace("<br/>", Environment.NewLine);

                    content = Regex.Replace(content, "<.*?>", string.Empty);
                    

                    News entity = news.ConvertToEntity();

                    entity.content = content.Substring(0, content.Length > 200 ? 200 : content.Length);

                    db.News.Add(entity);

                    db.News_body.Add(new News_body()
                    {
                       body = HttpUtility.HtmlEncode(news.Body),
                        News_Id = entity.News_Id
                    });

                    db.SaveChanges();

                }

                ViewBag.News_Id = new SelectList(db.News_body, "News_Id", "body", news.News_Id);
                
               // return View(news);
                return RedirectToAction("Index");
            }
            catch (Exception ex) {
                ViewBag.Message = "upload does not save to folder";
                return View();
            }
        }

        // GET: News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.News_Id = new SelectList(db.News_body, "News_Id", "body", news.News_Id);
            return View(news);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "News_Id,Title,content,Time_Created,ImgURL,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsActive,ShowImage")] News news)
        {
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.News_Id = new SelectList(db.News_body, "News_Id", "body", news.News_Id);
            return View(news);
        }

        // GET: News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }

            news.News_body.body = HttpUtility.HtmlDecode(news.News_body.body);
            return View(news);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
           // db.News.Remove(news);
            news.IsActive = false;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
