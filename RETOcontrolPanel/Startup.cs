﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RETOcontrolPanel.Startup))]
namespace RETOcontrolPanel
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
