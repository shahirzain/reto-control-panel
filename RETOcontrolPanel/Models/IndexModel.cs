﻿using Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web.Mvc;
namespace RETOcontrolPanel.Models
{
    public class IndexModel
    {
        [Key]
        public int News_Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }

        [Required]
        [StringLength(200)]
        public string content { get; set; }

        [StringLength(250)]
        public string ImgURL { get; set; }
        public IndexModel(News news)
        {
            this.News_Id = news.News_Id;
            this.Title = news.Title;
            this.content = news.content;
            this.ImgURL = news.ImgURL;
            // all other fields go here

        }
    }
}