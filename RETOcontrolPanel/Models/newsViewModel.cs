﻿namespace RETOcontrolPanel.Models
{
    using Entities;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    public partial class NewsViewModel
    {
       
        
        [Key]
        public int News_Id { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "TITLE")]
        public string Title { get; set; }

        //    [Required]
        //[StringLength(200)]
     //  public  string content { get; set; }

      //  public DateTime Time_Created { get; set; }

        [StringLength(250)]
        [Display(Name = "Image Path")]
        public string ImgURL { get; set; }

     //   [StringLength(10)]
        //public string CreatedBy { get; set; }

        //public DateTime CreatedDate { get; set; }

        //[StringLength(10)]
        //public string ModifiedBy { get; set; }

        //[StringLength(10)]
        //public string ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        [Display(Name = "Show Image?")]
        public bool? ShowImage { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        [Display(Name = "Content")]
        public string Body { get; set; }

        public NewsViewModel(News news)
        {
            this.News_Id = news.News_Id;
            this.Title = news.Title;
        //    this.Body = news.content;
            this.ImgURL = news.ImgURL;
            // all other fields go here
            
        }
        public NewsViewModel() {
        }

        public News ConvertToEntity() {
            string userid = System.Web.HttpContext.Current.User.Identity.GetUserId();
            News news = new News
            {
                News_Id = this.News_Id,
                Title = this.Title,
               // News_body = this.News_body,
               // content = this.Body,
                Time_Created = System.DateTime.Now,
                ImgURL = this.ImgURL,
                CreatedBy = String.IsNullOrEmpty(userid) ? "0" : userid,
                CreatedDate = System.DateTime.Now,
                ModifiedBy = String.IsNullOrEmpty(userid) ? "0" : userid,
                ModifiedDate = System.DateTime.Now.ToShortDateString().Substring(0,10),
                IsActive = this.IsActive,
                ShowImage = this.ShowImage
            };

            return news;
        }
    }

}
