namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("volunteer")]
    public partial class volunteer
    {
        [Key]
        public int vId { get; set; }

        [Required]
        [StringLength(20)]
        public string Fname { get; set; }

        [StringLength(20)]
        public string Lname { get; set; }

        [StringLength(35)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(8)]
        public string ZipCode { get; set; }

        [StringLength(250)]
        public string Remarks { get; set; }

        public bool? isActive { get; set; }
    }
}
