namespace Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=DefaultConnetion")
        {
        }

        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<BlogComment> BlogComments { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<News_body> News_body { get; set; }
        public virtual DbSet<NewsLetter> NewsLetters { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Program> Programs { get; set; }
        public virtual DbSet<Program_Body> Program_Body { get; set; }
        public virtual DbSet<ProgramCategory> ProgramCategories { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<ShadesVoting> ShadesVotings { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<volunteer> volunteers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Application>()
                .HasMany(e => e.Memberships)
                .WithRequired(e => e.Application)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Application>()
                .HasMany(e => e.Roles)
                .WithRequired(e => e.Application)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Application>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Application)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.img)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.Author)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.BlogComments)
                .WithRequired(e => e.Blog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BlogComment>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<BlogComment>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.Blogs)
                .WithRequired(e => e.Category)
                .HasForeignKey(e => e.catId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.ImgURL)
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.CreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.ModifiedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<News>()
                .Property(e => e.ModifiedDate);
                //.IsFixedLength();

            modelBuilder.Entity<News>()
                .HasOptional(e => e.News_body)
                .WithRequired(e => e.News);

            modelBuilder.Entity<News_body>()
                .Property(e => e.body)
                .IsUnicode(false);

            modelBuilder.Entity<NewsLetter>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.Short_Description)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.ImgURL)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.ImgURL_2)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Program>()
                .HasOptional(e => e.Program_Body)
                .WithRequired(e => e.Program);

            modelBuilder.Entity<Program_Body>()
                .Property(e => e.body)
                .IsUnicode(false);

            modelBuilder.Entity<ProgramCategory>()
                .Property(e => e.title)
                .IsUnicode(false);

            modelBuilder.Entity<ProgramCategory>()
                .Property(e => e.CreatedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProgramCategory>()
                .Property(e => e.ModifiedBy)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProgramCategory>()
                .HasMany(e => e.Programs)
                .WithRequired(e => e.ProgramCategory)
                .HasForeignKey(e => e.ProgramCategoryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.TeamId)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.FatherName)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.Class)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.School)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<ShadesVoting>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Membership)
                .WithRequired(e => e.User);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Profile)
                .WithRequired(e => e.User);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.Fname)
                .IsUnicode(false);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.Lname)
                .IsUnicode(false);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.ZipCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<volunteer>()
                .Property(e => e.Remarks)
                .IsUnicode(false);
        }
    }
}
