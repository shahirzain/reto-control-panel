namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NewsLetter")]
    public partial class NewsLetter
    {
        [Key]
        public int nlId { get; set; }

        [StringLength(35)]
        public string Email { get; set; }

        public DateTime? date { get; set; }

        public bool? unsubs { get; set; }
    }
}
