namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class News
    {
        [Key]
        public int News_Id { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Title")]
        public string Title { get; set; }

  //      [Required]
    //    [StringLength(200)]
        [UIHint("tinymce_jquery_full")]
        [Display(Name = "Content")]
        public string content { get; set; }

        [Display(Name = "Time Created")]
        public DateTime Time_Created { get; set; }

        [StringLength(250)]
        public string ImgURL { get; set; }

        [StringLength(10)]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [StringLength(10)]
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [StringLength(10)]
        [Display(Name = "Modified Date")]
        public String ModifiedDate { get; set; }


        public bool IsActive { get; set; }

        public bool? ShowImage { get; set; }

        public virtual News_body News_body { get; set; }

        
    }
}
