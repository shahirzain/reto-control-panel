namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ShadesVoting")]
    public partial class ShadesVoting
    {
        [Key]
        public int voteId { get; set; }

        [Required]
        [StringLength(5)]
        public string TeamId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string FatherName { get; set; }

        [StringLength(20)]
        public string Class { get; set; }

        [StringLength(50)]
        public string School { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        [Required]
        [StringLength(15)]
        public string Phone { get; set; }

        [Required]
        [StringLength(30)]
        public string Email { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        public DateTime TimeCreated { get; set; }

        public bool? IsActive { get; set; }
    }
}
