namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgramCategory")]
    public partial class ProgramCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProgramCategory()
        {
            Programs = new HashSet<Program>();
        }

        [Key]
        public int CategoryId { get; set; }

        [Required]
        [StringLength(80)]
        public string title { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(10)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime CreationDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ModificationDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Program> Programs { get; set; }
    }
}
