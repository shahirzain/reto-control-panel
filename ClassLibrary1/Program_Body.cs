namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Program_Body
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Program_Id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string body { get; set; }

        public virtual Program Program { get; set; }
    }
}
