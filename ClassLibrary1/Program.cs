namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Program")]
    public partial class Program
    {
        [Key]
        public int program_id { get; set; }

        public int ProgramCategoryId { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        public string Short_Description { get; set; }

        [Required]
        [StringLength(200)]
        public string content { get; set; }

        public DateTime Time_Created { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(39)]
        public string URL { get; set; }

        [Required]
        [StringLength(100)]
        public string ImgURL { get; set; }

        [Required]
        [StringLength(100)]
        public string ImgURL_2 { get; set; }

        [StringLength(200)]
        public string location { get; set; }

        public byte? status { get; set; }

        public bool? Featured { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Required]
        [StringLength(10)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public virtual Program_Body Program_Body { get; set; }

        public virtual ProgramCategory ProgramCategory { get; set; }
    }
}
