namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class News_body
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int News_Id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string body { get; set; }

        public virtual News News { get; set; }
    }
}
