namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogComment")]
    public partial class BlogComment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CommentId { get; set; }

        [Required]
        [StringLength(300)]
        public string content { get; set; }

        public DateTime PostTime { get; set; }

        [StringLength(15)]
        public string UserName { get; set; }

        public int blogId { get; set; }

        public bool? IsActive { get; set; }

        public virtual Blog Blog { get; set; }
    }
}
